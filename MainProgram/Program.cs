﻿using System;
using System.Linq;
using CalculatorLibrary;

namespace MainProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                var operation = GetSign();

                if (operation == "0")
                    return;

                double[] variables;
                if (IsBinary(operation))
                    variables = GettingInputData(2, new string[] { "x = ", "y = " });
                else
                    variables = GettingInputData(1, new string[] { "x = " });
                
                try
                {
                    Console.WriteLine(new Calculator(operation, variables).GetAnswer());
                }
                catch (DivideByZeroException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
            } while (true);
        }


        private static string GetSign()
        {
            string[] availableSigns = { "0", "+", "-", "*", "/", "%", "pow", "sqrt", "log",
                                        "tan", "sin", "cos", "!" };
            do
            {
                Console.WriteLine("Available operations: +, -, *, /, %, pow, sqrt, log, tan, sin, cos, ! or 0 to exit");
                Console.Write("Operation: ");
                string operation = Console.ReadLine();
                if (availableSigns.Contains(operation))
                    return operation;
                Console.WriteLine("Invalid operation");

            } while (true);
        }


        private static bool IsBinary(string operation)
        {
            string[] binary = { "+", "-", "*", "/", "%", "pow", "sqrt" };
            return binary.Contains(operation);
        }


        private static double[] GettingInputData(int count, string[] inputs)
        {
            double[] result = new double[count];
            for (int i = 0; i < count; ++i)
            {
                Console.Write(inputs[i]);
                while (!double.TryParse(Console.ReadLine(), out result[i])
                    || Math.Abs(result[i]) > 10000000000000000)
                    Console.Write("Invalid input format!\n" + inputs[i]);
            }
            return result;
        }
    }
}
