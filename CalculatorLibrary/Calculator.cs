﻿using System;

namespace CalculatorLibrary
{
    public class Calculator
    {
        private readonly string _operationNum;
        private readonly double[] _arguments;

        public Calculator(string operationNum, double[] arguments)
        {
            _operationNum = operationNum;
            _arguments = arguments;
        }

        public string GetAnswer()
        {
            switch (_operationNum)
            {
                case "+":
                    return $"{Sum(_arguments[0], _arguments[1]):f2}";
                case "-":
                    return $"{Subtraction(_arguments[0], _arguments[1]):f2}";
                case "/":
                    return $"{Division(_arguments[0], _arguments[1]):f2}";
                case "*":
                    return $"{Multiplication(_arguments[0], _arguments[1]):f2}";
                case "%":
                    return $"{Remainder(_arguments[0], _arguments[1]):f2}";
                case "pow":
                    return $"{Pow(_arguments[0], _arguments[1]):f2}";
                case "sqrt":
                    return $"{Sqrt(_arguments[0], _arguments[1]):f2}";
                case "log":
                    return $"{Log(_arguments[0]):f2}";
                case "tan":
                    return $"{Tan(_arguments[0]):f2}";
                case "sin":
                    return $"{Sin(_arguments[0]):f2}";
                case "cos":
                    return $"{Cos(_arguments[0]):f2}";
                case "!":
                    return $"{Factorial(_arguments[0]):f2}";
                default:
                    return "Invalid operation";
            }
        }

        private double Sum(double x, double y) => x + y;

        private double Subtraction(double x, double y) => x - y;

        private double Multiplication(double x, double y) => x * y;

        private double Division(double x, double y) => y != 0 ? x / y :
            throw new DivideByZeroException("Division by zero is not possible");

        private double Remainder(double x, double y) =>
            y == 0 ? throw new DivideByZeroException("Division by zero is not possible") : x % y;

        private double Pow(double x, double y)
        {
            if (x < 0)
                throw new ArgumentException("X must be positive");
            return Math.Pow(x, y);
        }

        private double Sqrt(double x, double y)
        {
            if (x < 0)
                throw new ArgumentException("X must be positive");
            return Math.Pow(x, 1 / y);
        }

        private double Log(double x)
        {
            if (x <= 0)
                throw new ArgumentException("X must be positive");
            return Math.Log(x);
        }

        private ulong Factorial(double x)
        {
            if (Math.Abs((int)x - x) > double.Epsilon)
                throw new ArgumentException("X must be Integer");
            if (x < 0)
                throw new ArgumentException("X must be positive");
            if (x > 15)
                throw new ArgumentException("Too big X");
            ulong y = 1;
            for (uint i = 1; i <= x; i++)
            {
                y *= i;
            }
            return y;
        }

        private double Cos(double x) => Math.Cos(x);

        private double Sin(double x) => Math.Sin(x);

        private double Tan(double x) => Math.Tan(x);
    }
}